---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_1fcbf5d495e6ada99ea017e9ae32b380 -->
## api/product/{product}
> Example request:

```bash
curl -X GET -G "http://localhost/api/product/java" 
```
```javascript
const url = new URL("http://localhost/api/product/java");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/product/java", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "product": {
        "id": 1003,
        "name": "Java",
        "slug": "java",
        "price": 3700,
        "index_image": "\/img\/books\/java.jpg",
        "publisher_id": 1,
        "created_at": "2019-05-22 12:50:41",
        "updated_at": "2019-05-22 12:50:41",
        "discount": {
            "id": 103,
            "name": "2+1 csomag kedvezmény",
            "type": "bundle",
            "value": null,
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "pivot": {
                "product_id": 1003,
                "discount_id": 103
            }
        },
        "new_price": 3700,
        "authors": [
            {
                "id": 3,
                "name": "Barry Burd",
                "slug": "barry-burd",
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "pivot": {
                    "product_id": 1003,
                    "author_id": 3
                }
            }
        ],
        "publisher": {
            "id": 1,
            "name": "PANEM",
            "slug": "panem",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41"
        },
        "discounts": [
            {
                "id": 103,
                "name": "2+1 csomag kedvezmény",
                "type": "bundle",
                "value": null,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "pivot": {
                    "product_id": 1003,
                    "discount_id": 103
                }
            }
        ]
    }
}
```

### HTTP Request
`GET api/product/{product}`


<!-- END_1fcbf5d495e6ada99ea017e9ae32b380 -->

<!-- START_dc538d69a8586a7a3c36d4393cee42e6 -->
## api/product
> Example request:

```bash
curl -X GET -G "http://localhost/api/product" 
```
```javascript
const url = new URL("http://localhost/api/product");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/product", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "products": [
        {
            "id": 1002,
            "name": "JavaScript kliens oldalon",
            "slug": "javascript-kliens-oldalon",
            "price": 2900,
            "index_image": "\/img\/books\/javascript_kliens_oldalon.jpg",
            "publisher_id": 2,
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "discount": {
                "id": 102,
                "name": "500-os kedvezmény a termék árából",
                "type": "fix",
                "value": 500,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "pivot": {
                    "product_id": 1002,
                    "discount_id": 102
                }
            },
            "new_price": 2400,
            "publisher": {
                "id": 2,
                "name": "BBS-INFO",
                "slug": "bbs-info",
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41"
            },
            "authors": [
                {
                    "id": 2,
                    "name": "Sikos László",
                    "slug": "sikos-laszlo",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1002,
                        "author_id": 2
                    }
                }
            ],
            "discounts": [
                {
                    "id": 102,
                    "name": "500-os kedvezmény a termék árából",
                    "type": "fix",
                    "value": 500,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1002,
                        "discount_id": 102
                    }
                }
            ]
        },
        {
            "id": 1003,
            "name": "Java",
            "slug": "java",
            "price": 3700,
            "index_image": "\/img\/books\/java.jpg",
            "publisher_id": 1,
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "discount": {
                "id": 103,
                "name": "2+1 csomag kedvezmény",
                "type": "bundle",
                "value": null,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "pivot": {
                    "product_id": 1003,
                    "discount_id": 103
                }
            },
            "new_price": 3700,
            "publisher": {
                "id": 1,
                "name": "PANEM",
                "slug": "panem",
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41"
            },
            "authors": [
                {
                    "id": 3,
                    "name": "Barry Burd",
                    "slug": "barry-burd",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1003,
                        "author_id": 3
                    }
                }
            ],
            "discounts": [
                {
                    "id": 103,
                    "name": "2+1 csomag kedvezmény",
                    "type": "bundle",
                    "value": null,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1003,
                        "discount_id": 103
                    }
                }
            ]
        },
        {
            "id": 1004,
            "name": "C# 2008",
            "slug": "c-2008",
            "price": 3700,
            "index_image": "\/img\/books\/csharp_2008.jpg",
            "publisher_id": 1,
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "discount": {
                "id": 103,
                "name": "2+1 csomag kedvezmény",
                "type": "bundle",
                "value": null,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "pivot": {
                    "product_id": 1004,
                    "discount_id": 103
                }
            },
            "new_price": 3700,
            "publisher": {
                "id": 1,
                "name": "PANEM",
                "slug": "panem",
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41"
            },
            "authors": [
                {
                    "id": 4,
                    "name": "Stephen Randy Davis",
                    "slug": "stephen-randy-davis",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1004,
                        "author_id": 4
                    }
                }
            ],
            "discounts": [
                {
                    "id": 103,
                    "name": "2+1 csomag kedvezmény",
                    "type": "bundle",
                    "value": null,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1004,
                        "discount_id": 103
                    }
                }
            ]
        }
    ],
    "empty": false
}
```

### HTTP Request
`GET api/product`


<!-- END_dc538d69a8586a7a3c36d4393cee42e6 -->

<!-- START_7d338992da825270c82e6303a9e79a69 -->
## api/category/all
> Example request:

```bash
curl -X GET -G "http://localhost/api/category/all" 
```
```javascript
const url = new URL("http://localhost/api/category/all");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/category/all", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
[
    {
        "id": 1,
        "name": "Könyv",
        "slug": "konyv",
        "created_at": "2019-05-22 12:50:41",
        "updated_at": "2019-05-22 12:50:41"
    }
]
```

### HTTP Request
`GET api/category/all`


<!-- END_7d338992da825270c82e6303a9e79a69 -->

<!-- START_d0f707385358373435c7b7937bcd2011 -->
## api/category/products
> Example request:

```bash
curl -X GET -G "http://localhost/api/category/products" 
```
```javascript
const url = new URL("http://localhost/api/category/products");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/category/products", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "category": {
        "id": 1,
        "name": "Könyv",
        "slug": "konyv",
        "created_at": "2019-05-22 12:50:41",
        "updated_at": "2019-05-22 12:50:41",
        "min_price": 2900,
        "max_price": 4500,
        "products": [
            {
                "id": 1002,
                "name": "JavaScript kliens oldalon",
                "slug": "javascript-kliens-oldalon",
                "price": 2900,
                "index_image": "\/img\/books\/javascript_kliens_oldalon.jpg",
                "publisher_id": 2,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "discount": {
                    "id": 102,
                    "name": "500-os kedvezmény a termék árából",
                    "type": "fix",
                    "value": 500,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1002,
                        "discount_id": 102
                    }
                },
                "new_price": 2400,
                "pivot": {
                    "category_id": 1,
                    "product_id": 1002
                },
                "publisher": {
                    "id": 2,
                    "name": "BBS-INFO",
                    "slug": "bbs-info",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41"
                },
                "authors": [
                    {
                        "id": 2,
                        "name": "Sikos László",
                        "slug": "sikos-laszlo",
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1002,
                            "author_id": 2
                        }
                    }
                ],
                "discounts": [
                    {
                        "id": 102,
                        "name": "500-os kedvezmény a termék árából",
                        "type": "fix",
                        "value": 500,
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1002,
                            "discount_id": 102
                        }
                    }
                ]
            },
            {
                "id": 1003,
                "name": "Java",
                "slug": "java",
                "price": 3700,
                "index_image": "\/img\/books\/java.jpg",
                "publisher_id": 1,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "discount": {
                    "id": 103,
                    "name": "2+1 csomag kedvezmény",
                    "type": "bundle",
                    "value": null,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1003,
                        "discount_id": 103
                    }
                },
                "new_price": 3700,
                "pivot": {
                    "category_id": 1,
                    "product_id": 1003
                },
                "publisher": {
                    "id": 1,
                    "name": "PANEM",
                    "slug": "panem",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41"
                },
                "authors": [
                    {
                        "id": 3,
                        "name": "Barry Burd",
                        "slug": "barry-burd",
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1003,
                            "author_id": 3
                        }
                    }
                ],
                "discounts": [
                    {
                        "id": 103,
                        "name": "2+1 csomag kedvezmény",
                        "type": "bundle",
                        "value": null,
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1003,
                            "discount_id": 103
                        }
                    }
                ]
            },
            {
                "id": 1004,
                "name": "C# 2008",
                "slug": "c-2008",
                "price": 3700,
                "index_image": "\/img\/books\/csharp_2008.jpg",
                "publisher_id": 1,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "discount": {
                    "id": 103,
                    "name": "2+1 csomag kedvezmény",
                    "type": "bundle",
                    "value": null,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1004,
                        "discount_id": 103
                    }
                },
                "new_price": 3700,
                "pivot": {
                    "category_id": 1,
                    "product_id": 1004
                },
                "publisher": {
                    "id": 1,
                    "name": "PANEM",
                    "slug": "panem",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41"
                },
                "authors": [
                    {
                        "id": 4,
                        "name": "Stephen Randy Davis",
                        "slug": "stephen-randy-davis",
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1004,
                            "author_id": 4
                        }
                    }
                ],
                "discounts": [
                    {
                        "id": 103,
                        "name": "2+1 csomag kedvezmény",
                        "type": "bundle",
                        "value": null,
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1004,
                            "discount_id": 103
                        }
                    }
                ]
            }
        ]
    },
    "publishers": [
        {
            "id": 1,
            "name": "PANEM",
            "slug": "panem",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 4,
            "authors": [
                1,
                3,
                4,
                5
            ],
            "products": [
                {
                    "id": 1001,
                    "name": "Dreamweaver CS4",
                    "slug": "dreamweaver-cs4",
                    "price": 3900,
                    "index_image": "\/img\/books\/dreamweaver_cs4.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "authors": [
                        {
                            "id": 1,
                            "name": "Janine Warner",
                            "slug": "janine-warner",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1001,
                                "author_id": 1
                            }
                        }
                    ]
                },
                {
                    "id": 1003,
                    "name": "Java",
                    "slug": "java",
                    "price": 3700,
                    "index_image": "\/img\/books\/java.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "authors": [
                        {
                            "id": 3,
                            "name": "Barry Burd",
                            "slug": "barry-burd",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1003,
                                "author_id": 3
                            }
                        }
                    ]
                },
                {
                    "id": 1004,
                    "name": "C# 2008",
                    "slug": "c-2008",
                    "price": 3700,
                    "index_image": "\/img\/books\/csharp_2008.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "authors": [
                        {
                            "id": 4,
                            "name": "Stephen Randy Davis",
                            "slug": "stephen-randy-davis",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1004,
                                "author_id": 4
                            }
                        }
                    ]
                },
                {
                    "id": 1005,
                    "name": "Az Ajax alapjai",
                    "slug": "az-ajax-alapjai",
                    "price": 4500,
                    "index_image": "\/img\/books\/az_ajax_alapjai.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "authors": [
                        {
                            "id": 5,
                            "name": "Joshua Eichorn",
                            "slug": "joshua-eichorn",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1005,
                                "author_id": 5
                            }
                        }
                    ]
                }
            ]
        },
        {
            "id": 2,
            "name": "BBS-INFO",
            "slug": "bbs-info",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "authors": [
                2
            ],
            "products": [
                {
                    "id": 1002,
                    "name": "JavaScript kliens oldalon",
                    "slug": "javascript-kliens-oldalon",
                    "price": 2900,
                    "index_image": "\/img\/books\/javascript_kliens_oldalon.jpg",
                    "publisher_id": 2,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "authors": [
                        {
                            "id": 2,
                            "name": "Sikos László",
                            "slug": "sikos-laszlo",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1002,
                                "author_id": 2
                            }
                        }
                    ]
                }
            ]
        },
        {
            "id": 3,
            "name": "TYPOTEX",
            "slug": "typotex",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "authors": [
                6,
                7,
                8
            ],
            "products": [
                {
                    "id": 1006,
                    "name": "Algoritmusok",
                    "slug": "algoritmusok",
                    "price": 3900,
                    "index_image": "\/img\/books\/algoritmusok.jpg",
                    "publisher_id": 3,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "authors": [
                        {
                            "id": 6,
                            "name": "Ivanyos Gábor",
                            "slug": "ivanyos-gabor",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1006,
                                "author_id": 6
                            }
                        },
                        {
                            "id": 7,
                            "name": "Rónyai Lajos",
                            "slug": "ronyai-lajos",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1006,
                                "author_id": 7
                            }
                        },
                        {
                            "id": 8,
                            "name": "Szabó Réka",
                            "slug": "szabo-reka",
                            "created_at": "2019-05-22 12:50:41",
                            "updated_at": "2019-05-22 12:50:41",
                            "pivot": {
                                "product_id": 1006,
                                "author_id": 8
                            }
                        }
                    ]
                }
            ]
        }
    ],
    "authors": [
        {
            "id": 1,
            "name": "Janine Warner",
            "slug": "janine-warner",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                1
            ],
            "products": [
                {
                    "id": 1001,
                    "name": "Dreamweaver CS4",
                    "slug": "dreamweaver-cs4",
                    "price": 3900,
                    "index_image": "\/img\/books\/dreamweaver_cs4.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 1,
                        "product_id": 1001
                    }
                }
            ]
        },
        {
            "id": 2,
            "name": "Sikos László",
            "slug": "sikos-laszlo",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                2
            ],
            "products": [
                {
                    "id": 1002,
                    "name": "JavaScript kliens oldalon",
                    "slug": "javascript-kliens-oldalon",
                    "price": 2900,
                    "index_image": "\/img\/books\/javascript_kliens_oldalon.jpg",
                    "publisher_id": 2,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 2,
                        "product_id": 1002
                    }
                }
            ]
        },
        {
            "id": 3,
            "name": "Barry Burd",
            "slug": "barry-burd",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                1
            ],
            "products": [
                {
                    "id": 1003,
                    "name": "Java",
                    "slug": "java",
                    "price": 3700,
                    "index_image": "\/img\/books\/java.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 3,
                        "product_id": 1003
                    }
                }
            ]
        },
        {
            "id": 4,
            "name": "Stephen Randy Davis",
            "slug": "stephen-randy-davis",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                1
            ],
            "products": [
                {
                    "id": 1004,
                    "name": "C# 2008",
                    "slug": "c-2008",
                    "price": 3700,
                    "index_image": "\/img\/books\/csharp_2008.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 4,
                        "product_id": 1004
                    }
                }
            ]
        },
        {
            "id": 5,
            "name": "Joshua Eichorn",
            "slug": "joshua-eichorn",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                1
            ],
            "products": [
                {
                    "id": 1005,
                    "name": "Az Ajax alapjai",
                    "slug": "az-ajax-alapjai",
                    "price": 4500,
                    "index_image": "\/img\/books\/az_ajax_alapjai.jpg",
                    "publisher_id": 1,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 5,
                        "product_id": 1005
                    }
                }
            ]
        },
        {
            "id": 6,
            "name": "Ivanyos Gábor",
            "slug": "ivanyos-gabor",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                3
            ],
            "products": [
                {
                    "id": 1006,
                    "name": "Algoritmusok",
                    "slug": "algoritmusok",
                    "price": 3900,
                    "index_image": "\/img\/books\/algoritmusok.jpg",
                    "publisher_id": 3,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 6,
                        "product_id": 1006
                    }
                }
            ]
        },
        {
            "id": 7,
            "name": "Rónyai Lajos",
            "slug": "ronyai-lajos",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                3
            ],
            "products": [
                {
                    "id": 1006,
                    "name": "Algoritmusok",
                    "slug": "algoritmusok",
                    "price": 3900,
                    "index_image": "\/img\/books\/algoritmusok.jpg",
                    "publisher_id": 3,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 7,
                        "product_id": 1006
                    }
                }
            ]
        },
        {
            "id": 8,
            "name": "Szabó Réka",
            "slug": "szabo-reka",
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "products_count": 1,
            "publishers": [
                3
            ],
            "products": [
                {
                    "id": 1006,
                    "name": "Algoritmusok",
                    "slug": "algoritmusok",
                    "price": 3900,
                    "index_image": "\/img\/books\/algoritmusok.jpg",
                    "publisher_id": 3,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "author_id": 8,
                        "product_id": 1006
                    }
                }
            ]
        }
    ],
    "empty": false
}
```

### HTTP Request
`GET api/category/products`


<!-- END_d0f707385358373435c7b7937bcd2011 -->

<!-- START_abcb18f6753f10de01226106f845124d -->
## api/category/products/filtered
> Example request:

```bash
curl -X GET -G "http://localhost/api/category/products/filtered" 
```
```javascript
const url = new URL("http://localhost/api/category/products/filtered");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/category/products/filtered", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/category/products/filtered`


<!-- END_abcb18f6753f10de01226106f845124d -->

<!-- START_428338ba7db0e8898bf193274deffeff -->
## api/author/products
> Example request:

```bash
curl -X GET -G "http://localhost/api/author/products" 
```
```javascript
const url = new URL("http://localhost/api/author/products");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/author/products", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "author": {
        "id": 2,
        "name": "Sikos László",
        "slug": "sikos-laszlo",
        "created_at": "2019-05-22 12:50:41",
        "updated_at": "2019-05-22 12:50:41",
        "products": [
            {
                "id": 1002,
                "name": "JavaScript kliens oldalon",
                "slug": "javascript-kliens-oldalon",
                "price": 2900,
                "index_image": "\/img\/books\/javascript_kliens_oldalon.jpg",
                "publisher_id": 2,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "discount": {
                    "id": 102,
                    "name": "500-os kedvezmény a termék árából",
                    "type": "fix",
                    "value": 500,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1002,
                        "discount_id": 102
                    }
                },
                "new_price": 2400,
                "pivot": {
                    "author_id": 2,
                    "product_id": 1002
                },
                "publisher": {
                    "id": 2,
                    "name": "BBS-INFO",
                    "slug": "bbs-info",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41"
                },
                "discounts": [
                    {
                        "id": 102,
                        "name": "500-os kedvezmény a termék árából",
                        "type": "fix",
                        "value": 500,
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1002,
                            "discount_id": 102
                        }
                    }
                ]
            }
        ]
    },
    "empty": true
}
```

### HTTP Request
`GET api/author/products`


<!-- END_428338ba7db0e8898bf193274deffeff -->

<!-- START_c9a7fde57fcde616815b7eca2a936408 -->
## api/publisher/products
> Example request:

```bash
curl -X GET -G "http://localhost/api/publisher/products" 
```
```javascript
const url = new URL("http://localhost/api/publisher/products");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/publisher/products", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "publisher": {
        "id": 1,
        "name": "PANEM",
        "slug": "panem",
        "created_at": "2019-05-22 12:50:41",
        "updated_at": "2019-05-22 12:50:41",
        "products": [
            {
                "id": 1001,
                "name": "Dreamweaver CS4",
                "slug": "dreamweaver-cs4",
                "price": 3900,
                "index_image": "\/img\/books\/dreamweaver_cs4.jpg",
                "publisher_id": 1,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "discount": {
                    "id": 103,
                    "name": "2+1 csomag kedvezmény",
                    "type": "bundle",
                    "value": null,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1001,
                        "discount_id": 103
                    }
                },
                "new_price": 3900,
                "authors": [
                    {
                        "id": 1,
                        "name": "Janine Warner",
                        "slug": "janine-warner",
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1001,
                            "author_id": 1
                        }
                    }
                ],
                "discounts": [
                    {
                        "id": 103,
                        "name": "2+1 csomag kedvezmény",
                        "type": "bundle",
                        "value": null,
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1001,
                            "discount_id": 103
                        }
                    }
                ]
            },
            {
                "id": 1003,
                "name": "Java",
                "slug": "java",
                "price": 3700,
                "index_image": "\/img\/books\/java.jpg",
                "publisher_id": 1,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "discount": {
                    "id": 103,
                    "name": "2+1 csomag kedvezmény",
                    "type": "bundle",
                    "value": null,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1003,
                        "discount_id": 103
                    }
                },
                "new_price": 3700,
                "authors": [
                    {
                        "id": 3,
                        "name": "Barry Burd",
                        "slug": "barry-burd",
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1003,
                            "author_id": 3
                        }
                    }
                ],
                "discounts": [
                    {
                        "id": 103,
                        "name": "2+1 csomag kedvezmény",
                        "type": "bundle",
                        "value": null,
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1003,
                            "discount_id": 103
                        }
                    }
                ]
            },
            {
                "id": 1004,
                "name": "C# 2008",
                "slug": "c-2008",
                "price": 3700,
                "index_image": "\/img\/books\/csharp_2008.jpg",
                "publisher_id": 1,
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41",
                "discount": {
                    "id": 103,
                    "name": "2+1 csomag kedvezmény",
                    "type": "bundle",
                    "value": null,
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1004,
                        "discount_id": 103
                    }
                },
                "new_price": 3700,
                "authors": [
                    {
                        "id": 4,
                        "name": "Stephen Randy Davis",
                        "slug": "stephen-randy-davis",
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1004,
                            "author_id": 4
                        }
                    }
                ],
                "discounts": [
                    {
                        "id": 103,
                        "name": "2+1 csomag kedvezmény",
                        "type": "bundle",
                        "value": null,
                        "created_at": "2019-05-22 12:50:41",
                        "updated_at": "2019-05-22 12:50:41",
                        "pivot": {
                            "product_id": 1004,
                            "discount_id": 103
                        }
                    }
                ]
            }
        ]
    },
    "empty": false
}
```

### HTTP Request
`GET api/publisher/products`


<!-- END_c9a7fde57fcde616815b7eca2a936408 -->

<!-- START_6c1682f4a40c1555254df708bf6b4a71 -->
## api/cart
> Example request:

```bash
curl -X GET -G "http://localhost/api/cart" 
```
```javascript
const url = new URL("http://localhost/api/cart");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/cart", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "products": [],
    "cart": [],
    "total": 0,
    "discounted": 0,
    "quantity": 0
}
```

### HTTP Request
`GET api/cart`


<!-- END_6c1682f4a40c1555254df708bf6b4a71 -->

<!-- START_ffc9879f3ac5b6304e7047c50762e044 -->
## api/cart
> Example request:

```bash
curl -X POST "http://localhost/api/cart" 
```
```javascript
const url = new URL("http://localhost/api/cart");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->post("api/cart", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "message": "A terméket sikeresen hozzáadtuk kosarához"
}
```

### HTTP Request
`POST api/cart`


<!-- END_ffc9879f3ac5b6304e7047c50762e044 -->

<!-- START_cee643dbb43e39f459054ea303d45955 -->
## api/cart/{product}
> Example request:

```bash
curl -X PUT "http://localhost/api/cart/java" 
```
```javascript
const url = new URL("http://localhost/api/cart/java");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->put("api/cart/java", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "products": [
        {
            "id": 1003,
            "name": "Java",
            "slug": "java",
            "price": 3700,
            "index_image": "\/img\/books\/java.jpg",
            "publisher_id": 1,
            "created_at": "2019-05-22 12:50:41",
            "updated_at": "2019-05-22 12:50:41",
            "cart_price": 7400,
            "publisher": {
                "id": 1,
                "name": "PANEM",
                "slug": "panem",
                "created_at": "2019-05-22 12:50:41",
                "updated_at": "2019-05-22 12:50:41"
            },
            "authors": [
                {
                    "id": 3,
                    "name": "Barry Burd",
                    "slug": "barry-burd",
                    "created_at": "2019-05-22 12:50:41",
                    "updated_at": "2019-05-22 12:50:41",
                    "pivot": {
                        "product_id": 1003,
                        "author_id": 3
                    }
                }
            ]
        }
    ],
    "cart": {
        "java": {
            "qty": 2
        }
    },
    "total": 7400,
    "discounted": 7400,
    "quantity": 2
}
```

### HTTP Request
`PUT api/cart/{product}`


<!-- END_cee643dbb43e39f459054ea303d45955 -->

<!-- START_64fa8ff4c1e0f70f648d9cd568a2f06b -->
## api/cart/{product}
> Example request:

```bash
curl -X DELETE "http://localhost/api/cart/java" 
```
```javascript
const url = new URL("http://localhost/api/cart/java");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->delete("api/cart/java", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/cart/{product}`


<!-- END_64fa8ff4c1e0f70f648d9cd568a2f06b -->

<!-- START_4b262cbdf4607514a66a088ed8f554a9 -->
## api/cart/empty
> Example request:

```bash
curl -X GET -G "http://localhost/api/cart/empty" 
```
```javascript
const url = new URL("http://localhost/api/cart/empty");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```
```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/cart/empty", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "cart": {
        "products": [],
        "cart": [],
        "total": 0,
        "discounted": 0,
        "quantity": 0
    },
    "message": "A kosár sikeresen ürítve"
}
```

### HTTP Request
`GET api/cart/empty`


<!-- END_4b262cbdf4607514a66a088ed8f554a9 -->


