-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.26-0ubuntu0.18.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Verzió:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for tábla bigfish.authors
CREATE TABLE IF NOT EXISTS `authors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.authors: ~8 rows (approximately)
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Janine Warner', 'janine-warner', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(2, 'Sikos László', 'sikos-laszlo', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(3, 'Barry Burd', 'barry-burd', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(4, 'Stephen Randy Davis', 'stephen-randy-davis', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(5, 'Joshua Eichorn', 'joshua-eichorn', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(6, 'Ivanyos Gábor', 'ivanyos-gabor', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(7, 'Rónyai Lajos', 'ronyai-lajos', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(8, 'Szabó Réka', 'szabo-reka', '2019-05-27 14:54:30', '2019-05-27 14:54:30');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.categories: ~1 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Könyv', 'konyv', '2019-05-27 14:54:30', '2019-05-27 14:54:30');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.discounts
CREATE TABLE IF NOT EXISTS `discounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.discounts: ~3 rows (approximately)
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
INSERT INTO `discounts` (`id`, `name`, `type`, `value`, `created_at`, `updated_at`) VALUES
	(101, '10%-os kedvezmény a termék árából', 'percent', 10, '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(102, '500-os kedvezmény a termék árából', 'fix', 500, '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(103, '2+1 csomag kedvezmény', 'bundle', NULL, '2019-05-27 14:54:30', '2019-05-27 14:54:30');
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.migrations: ~5 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2019_05_15_190028_create_products_table', 1),
	(2, '2019_05_15_190136_create_categories_table', 1),
	(3, '2019_05_16_101128_create_authors_table', 1),
	(4, '2019_05_16_101202_create_publishers_table', 1),
	(5, '2019_05_21_095608_create_discounts_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `index_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=1007 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.products: ~6 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `slug`, `price`, `index_image`, `publisher_id`, `created_at`, `updated_at`) VALUES
	(1001, 'Dreamweaver CS4', 'dreamweaver-cs4', 3900, '/img/books/dreamweaver_cs4.jpg', 1, '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(1002, 'JavaScript kliens oldalon', 'javascript-kliens-oldalon', 2900, '/img/books/javascript_kliens_oldalon.jpg', 2, '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(1003, 'Java', 'java', 3700, '/img/books/java.jpg', 1, '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(1004, 'C# 2008', 'c-2008', 3700, '/img/books/csharp_2008.jpg', 1, '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(1005, 'Az Ajax alapjai', 'az-ajax-alapjai', 4500, '/img/books/az_ajax_alapjai.jpg', 1, '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(1006, 'Algoritmusok', 'algoritmusok', 3900, '/img/books/algoritmusok.jpg', 3, '2019-05-27 14:54:30', '2019-05-27 14:54:30');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.product_author
CREATE TABLE IF NOT EXISTS `product_author` (
  `author_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  KEY `product_author_author_id_foreign` (`author_id`),
  KEY `product_author_product_id_foreign` (`product_id`),
  CONSTRAINT `product_author_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_author_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.product_author: ~8 rows (approximately)
/*!40000 ALTER TABLE `product_author` DISABLE KEYS */;
INSERT INTO `product_author` (`author_id`, `product_id`) VALUES
	(1, 1001),
	(2, 1002),
	(3, 1003),
	(4, 1004),
	(5, 1005),
	(6, 1006),
	(7, 1006),
	(8, 1006);
/*!40000 ALTER TABLE `product_author` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `category_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  KEY `product_category_category_id_foreign` (`category_id`),
  KEY `product_category_product_id_foreign` (`product_id`),
  CONSTRAINT `product_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_category_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.product_category: ~6 rows (approximately)
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` (`category_id`, `product_id`) VALUES
	(1, 1001),
	(1, 1002),
	(1, 1003),
	(1, 1004),
	(1, 1005),
	(1, 1006);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.product_discount
CREATE TABLE IF NOT EXISTS `product_discount` (
  `discount_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  KEY `product_discount_discount_id_foreign` (`discount_id`),
  KEY `product_discount_product_id_foreign` (`product_id`),
  CONSTRAINT `product_discount_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_discount_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.product_discount: ~6 rows (approximately)
/*!40000 ALTER TABLE `product_discount` DISABLE KEYS */;
INSERT INTO `product_discount` (`discount_id`, `product_id`) VALUES
	(101, 1006),
	(102, 1002),
	(103, 1001),
	(103, 1003),
	(103, 1004),
	(103, 1005);
/*!40000 ALTER TABLE `product_discount` ENABLE KEYS */;

-- Dumping structure for tábla bigfish.publishers
CREATE TABLE IF NOT EXISTS `publishers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bigfish.publishers: ~3 rows (approximately)
/*!40000 ALTER TABLE `publishers` DISABLE KEYS */;
INSERT INTO `publishers` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'PANEM', 'panem', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(2, 'BBS-INFO', 'bbs-info', '2019-05-27 14:54:30', '2019-05-27 14:54:30'),
	(3, 'TYPOTEX', 'typotex', '2019-05-27 14:54:30', '2019-05-27 14:54:30');
/*!40000 ALTER TABLE `publishers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
