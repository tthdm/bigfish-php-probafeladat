<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{subs}', function () {
    return view('home')
        ->with('localization', ExportLocalization::export()->toArray());
})->name('home')->where(['subs' => '(?!api).*']);

Route::group(['prefix' => 'api', 'as' => 'api::', 'namespace' => 'Api'], function () {

    Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
        Route::get('/{product}', 'ProductController@show')->name('show');
        Route::get('/', 'ProductController@products')->name('products');
    });

    Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
        Route::get('/all', 'CategoryController@all')->name('all');
        Route::get('/products', 'CategoryController@products')->name('products');
        Route::get('/products/filtered', 'CategoryController@filterProducts')->name('products/filtered');
    });

    Route::group(['prefix' => 'author', 'as' => 'author.'], function () {
        Route::get('/products', 'AuthorController@products')->name('products');
        Route::get('/', 'AuthorController@authors')->name('authors');
    });

    Route::group(['prefix' => 'publisher', 'as' => 'publisher.'], function () {
        Route::get('/products', 'PublisherController@products')->name('products');
        Route::get('/', 'PublisherController@publishers')->name('publishers');
    });

    Route::group(['prefix' => 'cart', 'as' => 'cart.'], function () {
        Route::get('/', 'CartController@all')->name('all');
        Route::post('/', 'CartController@store')->name('store');
        Route::put('/{product}', 'CartController@update')->name('update');
        Route::delete('/{product}', 'CartController@destroy')->name('destroy');
        Route::get('/empty', 'CartController@empty')->name('empty');
    });

});