<p align="center"><img src="http://bigfish.magnahost.tk/storage/git_logo.svg"></p>


## Röviden az applikációról

A próbafeladat háttérrendszerét Laravel keretrendszer segítségével készítettem el, melyet a Vue.js nevű frontend keretrendszerrel jelenítek meg és kérdezek le.

Ahogyan a feladatban kérve volt, a kért mellékletek az alábbi helyen találhatók:


- SQL dump a **docs/feladat.sql** alatt található.
- A generált dokumentáció **docs/index.html** alatt található.
- Adatséma és rendszer tervből az adatséma lentebb görgetve található, rendszertervet nem véltem szükségesnek, hiszen a Laravel sajátos MVC szerkezeti mintájára épült.
- Telepítési útmutató lentebb görgetve található

## UML Adatséma:
<img src="http://bigfish.magnahost.tk/storage/uml_database.png">

## Telepítési útmutató

Hozza létre az alkalmazásnak szánt mappát (pl. laravel)
```bash
$ mkdir laravel
```
Lépjen a mappába
```bash
$ cd laravel
```
Töltse le az applikációt git segítségével:
```bash
$ git clone https://gitlab.com/tthdm/bigfish-php-probafeladat.git .
```
Telepítse a szükséges csomagokat:
```bash
$ composer install
```
Hozza létre konfigurációt az alábbi két opció közül az egyikkel
```bash
$ sudo ./createConfig.sh <adatbázis> <felhasználónév> <jelszó>
```
Melyre egy példa
```bash
$ sudo ./createConfig.sh bigfish bigfish_client Bigfish123
```
**vagy**

Nevezze át a **.env.example** fájlt **.env**-re, majd a benne található **DB_DATABASE**, **DB_USERNAME**, **DB_PASSWORD** soroknál az egyenlőségjelet követően értelemszerűen töltse ki.  

**pl.**:  

DB_DATABASE=**bigfish**  
DB_USERNAME=**bigfish_client**  
DB_PASSWORD=**Bigfish123**

Generálja le az applikáció kulcsát a konfigurációs fájlba a következő parancs segítségével:

```bash
$ php artisan key:generate
```

Mindezután a mellékelt SQL fájlt importálja adatbázisába, vagy használja az alábbi parancsokat az adatbázis struktúra létrehozására, továbbá a tesztadatok feltöltésére
```bash
$ php artisan migrate && php artisan db:seed
```

## Ajánlott NGINX konfiguráció
Hozza létre az NGINX ajánlott konfigurációt egy egyszerű paranccsal
```bash
$ sudo ./createNginxConfig.sh <domain> <célmappa>
```
Erre egy példa
```bash
$ sudo ./createNginxConfig.sh example.com /var/www/html/example.com/public_html
```
Ha mégis manuális konfiguráció mellett dönt, akkor az alábbi konfiguráció esetén a **$domain** és a **$root** átírásával könnyedén végezhet konfigurációjának elkészítésével
```apacheconfig
server {
        listen 80;
    
        server_name $domain;
    
        index  index.php;
    
        charset utf-8;
    
        root  $root/public;
    
        gzip on;
        gzip_static on;
        gzip_http_version 1.0;
        gzip_disable "MSIE [1-6].";
        gzip_vary on;
        gzip_comp_level 9;
        gzip_proxied any;
        gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript image/svg+xml;
    
        fastcgi_intercept_errors off;
        fastcgi_buffers 8 16k;
        fastcgi_buffer_size 32k;
        fastcgi_read_timeout 180;
    
        client_max_body_size 120M;
    
        location / {
            try_files $uri $uri/ /index.php?$args;
        }
    
        location ~ \.php$ {
    
            fastcgi_pass unix:/run/php/php7.2-fpm.sock;
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
    
            include        fastcgi_params;
        }
    
        location ~* \.ico$ {
    
            expires 1w;
            access_log off;
        }
    
        location ~* \.(?:jpg|jpeg|gif|png|ico|gz|svg|svgz|ttf|otf|woff|eot|mp4|ogg|ogv|webm)$ {
    
            try_files $uri $uri/ /index.php?$query_string;
    
            access_log off;
            log_not_found off;
        }
    
        location ~* \.(?:css|js)$ {
    
            try_files $uri $uri/ /index.php?$query_string;
    
            access_log off;
            log_not_found off;
        }
    
        add_header "X-UA-Compatible" "IE=Edge,chrome=1";
    
    }
```
