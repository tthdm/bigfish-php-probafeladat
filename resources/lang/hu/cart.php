<?php

return [
    'menu' => [
        'title' => 'Kosarad tartalma',
        'sections' => [
            'emptyCart' => [
                'title' => 'Üres a kosarad!',
                'description' => 'Nézz körül, hátha találsz megfelelő könyvet!',
            ],
            'verdict' => [
                'text' => [
                    'total' => 'Összesen'
                ]
            ]
        ]
    ],
];