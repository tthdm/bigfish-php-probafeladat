<?php

return [
    'moreProducts' => [
        'title' => 'További termékek',
        'loading' => 'Betöltés...'
    ],
    'refine' => [
        'title' => 'Szűrés',
    ],
    'fullCart' => [
        'title' => 'Teljes kosár',
    ],
    'addToCart' => [
        'title' => 'Kosárba tesz'
    ],
    'emptyCart' => [
        'title' => 'Kosár ürítése'
    ]
];