<?php

return [
    'productsEmpty' => 'Nincs több termék',
    'errorOccurred' => 'Hiba történt',
    'success' => [
        'cart' => [
            'add' => 'A terméket sikeresen hozzáadtuk kosarához',
            'remove' => 'A terméket sikeresen eltávolítottuk a kosárból',
            'empty' => 'A kosár sikeresen ürítve'
        ]
    ],
    'errors' => [
        'cart' => [
            'add' => [
                'productNotFound' => 'A termék nem található'
            ],
            'update' => [
                'emptyCart' => 'A kosár üres, először adjon hozzá terméket',
                'productNotFound' => 'A termék nem található',
                'productNotFoundInCart' => 'A termék nem található a kosárban',
                'ratherUseDelete' => 'Inkább használja a törlés gombot',
                'badOperation' => 'Nem megfelelő művelet'
            ],
            'remove' => [
                'emptyCart' => 'A kosár üres, először adjon hozzá terméket',
                'productNotFound' => 'A termék nem található',
                'productNotFoundInCart' => 'A termék nem található a kosárban',
            ],
            'empty' => [
                'couldntEmptyCart' => 'A kosár ürítése közben hiba lépett fel'
            ]
        ]
    ]
];