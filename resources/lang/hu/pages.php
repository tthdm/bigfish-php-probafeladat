<?php

return [
    'home' => [
        'title' => 'Főoldal',
        'description' => 'Termékek',
        'sections' => [
            'products' => [
                'title' => 'Termékek',
            ],
            'categories' => [
                'title' => 'Termékek',
            ]
        ],
    ],
    'category' => [
        'description' => 'kategória termékei',
        'sections' => [
            'products' => [
                'title' => 'Termékek',
            ],
            'refine' => [
                'title' => 'Szűrés',
                'resetFilter' => 'Szűrés törlése'
            ],
            'priceRange' => [
                'title' => 'Ár'
            ],
            'publisher' => [
                'title' => 'Kiadó'
            ],
            'author' => [
                'title' => 'Szerző'
            ]
        ]
    ],
    'author' => [
        'description' => 'szerzőtől',
        'sections' => [
            'products' => [
                'title' => 'Termékek',
            ],
        ]
    ],
    'publisher' => [
        'description' => 'kiadótól',
        'sections' => [
            'products' => [
                'title' => 'Termékek',
            ],
        ]
    ],
    'product' => [
        'sections' => [
            'description' => [
                'title' => 'Leírás',
                'text' => [
                    'publisher' => 'Kiadó',
                    'author' => 'Szerző',
                ]
            ],
        ]
    ],
    'cart' => [
        'title' => 'Kosár',
        'text' => [
            'total' => 'Összesen',
            'totalWithoutDiscount' => 'Kedvezmény nélkül',
            'totalDiscount' => 'Kedvezmény értéke',
        ],
        'table' => [
            'product' => 'Termék',
            'price' => 'Egységár',
            'quantity' => 'Darab',
            'totalPrice' => 'Összesen'

        ]
    ],
    '404' => [
        'title' => 'Az oldal nem található',
        'description' => 'Hoppá! Úgy néz ki eltévedtél.',
    ]


];