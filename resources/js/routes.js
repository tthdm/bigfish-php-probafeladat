import Home from './Components/Pages/Home'
import Category from './Components/Pages/Category'
import Author from './Components/Pages/Author'
import Publisher from './Components/Pages/Publisher'
import Product from './Components/Pages/Product'
import Cart from './Components/Pages/Cart'

import NotFound from './Components/Errors/NotFound'

export default [
    { path: '/', name: 'home', component: Home},
    { path: '/kategoria/:category', name: 'category', component: Category},
    { path: '/szerzo/:author', name: 'author', component: Author},
    { path: '/kiado/:publisher', name: 'publisher', component: Publisher},
    { path: '/termek/:product', name: 'product', component: Product},
    { path: '/kosar', name: 'cart', component: Cart},
    { path: '/404', component: NotFound },
    { path: '*', redirect: '/404' },
]