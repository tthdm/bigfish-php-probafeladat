import Vue from 'vue'
import VueRouter from 'vue-router'
import VueHead from 'vue-head'
import VueI18n from 'vue-i18n'

import App from './Components/App'
import routes from './routes'

require('./bootstrap');

const locale = window.locale;
const messages = window.localization;
const app_name = window.app_name;
const domain = window.domain;

Vue.prototype.lodash = window._;

const i18n = new VueI18n({
    locale: locale,
    messages,
});

Vue.use(VueHead, {
    separator: '›',
    complement: app_name,
});

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: routes,
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});

window.onload = function () {
    const app = new Vue({
        el: '#app',
        router,
        i18n,
        domain,
        render: h => h(App)
    });
};

$(document).ready(function () {
    $.getScript('/js/core/popper.min.js');
    $.getScript('/js/bootstrap-material-design.min.js');
    $.getScript('/js/modernizr.js');
    $.getScript('/js/material-kit.min.js');
    $.getScript('/js/plugins/moment.min.js');
    $.getScript('/js/plugins/bootstrap-datetimepicker.min.js');
    $.getScript('/js/plugins/nouislider.min.js');
    $.getScript('/js/plugins/bootstrap-selectpicker.js');
    $.getScript('/js/plugins/bootstrap-tagsinput.js');
    $.getScript('/js/plugins/bootstrap-notify.min.js');
    $.getScript('/js/plugins/jasny-bootstrap.min.js');
    $.getScript('/js/plugins/jquery.flexisel.js');
});