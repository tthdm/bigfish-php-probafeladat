<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Betöltés... › {{ config('app.name') }}</title>
    <link rel="apple-touch-icon" href="/img/favicons/apple-icon.png">
    <link rel="icon" href="/img/favicons/favicon.png">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/css/material-kit.min.css?v=2.0.2">
    <link rel="stylesheet" href="/css/animation.css">
    <link rel="stylesheet" href="/css/custom_alert.css">
    <link rel="stylesheet" href="/css/custom.css">

</head>
<body>

    <div id="app"></div>


    <script>

        window.locale = "{{ config('app.locale') }}";
        window.localization = @json($localization);
        window.app_name = "{{ config('app.name') }}";
        window.domain = "{{ url()->to('/') }}";

    </script>

    <script src="/js/app.js"></script>

</body>
</html>