<?php

namespace App\Http\Middleware;

use Closure;
use App\Category;
use App\Cart;

class SharedVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        \View::share('menu', Category::all());
        //\View::share('cart', Cart::get());

        return $next($request);
    }
}
