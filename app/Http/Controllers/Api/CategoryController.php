<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Category\CategoryRepository;

class CategoryController extends Controller
{

    protected $category;

    public function __construct(CategoryRepository $repository)
    {
        $this->category = $repository;
    }


    public function products(Request $request)
    {

        $productsCount = $this->category->countProducts($request->category);

        $empty = $productsCount <= $request->offset+3;

        $category = $this->category->withProducts($request->category, 3, 0);

        return response()->json([
            'category' => $category,
            'empty' => $empty
        ]);

    }

    public function filterProducts(Request $request)
    {

        $products = $this->category->filterProducts(
            $request->category,
            $request->priceRange['min'],
            $request->priceRange['max'],
            $request->author,
            $request->publisher,
            3,
            $request->offset);

        $empty = $products->countAll <= $request->offset+3;

        return response()->json([
            'products' => $products,
            'empty' => $empty
        ]);

    }

    public function all()
    {

        $categories = $this->category->all();

        return response()->json($categories);

    }

}
