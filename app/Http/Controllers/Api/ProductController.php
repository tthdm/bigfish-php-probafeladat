<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Product\ProductRepository;

class ProductController extends Controller
{

    protected $product;

    public function __construct(ProductRepository $repository)
    {
        $this->product = $repository;
    }

    public function show($slug)
    {

        $product = $this->product->getBySlug($slug);

        return response()->json([
            'product' => $product
        ]);

    }

    public function products(Request $request)
    {

        $products = $this->product->getPaginated(3, $request->offset);
        $productsCount = $this->product->count();

        $empty = $productsCount <= $request->offset+3;

        return response()->json([
            'products' => $products,
            'empty' => $empty
        ]);

    }

}
