<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Author\AuthorRepository;

class AuthorController extends Controller
{

    protected $author;

    public function __construct(AuthorRepository $repository)
    {
        $this->author = $repository;
    }

    public function products(Request $request)
    {

        $author = $this->author->paginateProductsWithPublisher($request->author, 3, $request->offset);
        $productsCount = $this->author->countProducts($request->author);

        if($author === null) {
            return response()->json([
                'author' => $author,
            ]);
        }

        $empty = $productsCount <= $request->offset+3;

        return response()->json([
            'author' => $author,
            'empty' => $empty
        ]);

    }

    public function authors()
    {
        $authors =  $this->author->allWithProducts();

        return response()->json([
            'authors' => $authors
        ]);
    }

}
