<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cart;
use Arr;
use DB;

class CartController extends Controller
{

    public function all() {

        $cart = Cart::get();

        return response()->json($cart);

    }

    public function store(Request $request)
    {

        $cart = Cart::add($request->product);

        if($cart['error']) {
            return response()->json([
                'title' => trans('messages.errorOccurred'),
                'message' => $cart['message']
            ], 500);
        } else {
            return response()->json([
                'message' => trans('messages.success.cart.add'),
            ]);
        }

    }

    public function update($product, Request $request)
    {

        $cart = Cart::update($product, $request->operation);

        if($cart['error']) {
            return response()->json([
                'title' => trans('messages.errorOccurred'),
                'message' => $cart['message']
            ], 500);
        } else {
            return response()->json($cart['session']);
        }

    }

    public function destroy($product)
    {

        $cart = Cart::remove($product);

        if($cart['error']) {
            return response()->json([
                'title' => trans('messages.errorOccurred'),
                'message' => $cart['message']
            ], 500);
        } else {
            return response()->json($cart['session']);
        }

    }

    public function empty() {

        $cart = Cart::empty();

        if($cart['error']) {
            return response()->json(['title' => trans('messages.errorOccurred'), 'message' => $cart['message']], 500);
        } else {
            return response()->json([
                'cart' => Cart::get(),
                'message' => trans('messages.success.cart.empty')
            ]);
        }

    }

}
