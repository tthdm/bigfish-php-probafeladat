<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Publisher\PublisherRepository;

class PublisherController extends Controller
{

    protected $publisher;

    public function __construct(PublisherRepository $repository)
    {
        $this->publisher = $repository;
    }

    public function products(Request $request)
    {

        $publisher = $this->publisher->paginateProductsWithAuthors($request->publisher, 3, $request->offset);
        $productsCount = $this->publisher->countProducts($request->publisher);

        if($publisher === null) {
            return response()->json([
                'publisher' => $publisher,
            ]);
        }

        $empty = $productsCount <= $request->offset+3;

        return response()->json([
            'publisher' => $publisher,
            'empty' => $empty
        ]);

    }

    public function publishers()
    {
        $publishers =  $this->publisher->allWithProducts();

        return response()->json([
            'publishers' => $publishers
        ]);
    }

}
