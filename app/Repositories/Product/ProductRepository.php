<?php

namespace App\Repositories\Product;

use App\Product;
use App\Helpers\DiscountHelper;

class ProductRepository implements ProductRepositoryInterface
{

    /**
     * The model of products
     * @var Product
     */
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getPaginated($limit, $offset = 0)
    {
        $products = $this->model->orderBy('price')
                        ->with(['publisher', 'authors'])
                        ->limit($limit)
                        ->offset($offset)
                        ->get();

        DiscountHelper::processProductsPrice($products);

        return $products;
    }

    public function getBySlug($slug)
    {
        $product = $this->findBySlug($slug)->with(['authors', 'publisher'])->first();

        DiscountHelper::attachDiscountedPrice($product);

        return $product;

    }

    public function count()
    {
        return $this->model->count();
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug);
    }


}