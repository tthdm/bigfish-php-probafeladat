<?php

namespace App\Repositories\Product;

use App\Repositories\RepositoryInterface;

interface ProductRepositoryInterface extends RepositoryInterface
{

    public function getPaginated($limit, $offset);

    public function getBySlug($slug);

    public function count();

    public function findBySlug($slug);

}