<?php

namespace App\Repositories\Category;

use App\Repositories\RepositoryInterface;

interface CategoryRepositoryInterface extends RepositoryInterface
{

    public function withProducts($slug, $limit, $offset);

    public function filterProducts($slug, $minPrice, $maxPrice, $author, $publisher, $limit, $offset);

    public function countProducts($slug);

    public function findBySlug($slug);

    public function all();

}