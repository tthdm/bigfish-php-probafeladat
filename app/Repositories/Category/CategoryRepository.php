<?php

namespace App\Repositories\Category;

use App\Category;
use App\Helpers\DiscountHelper;

class CategoryRepository
{

    /**
     * The model of categories
     * @var Category
     */
    protected $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function withProducts($slug, $limit, $offset = 0)
    {
        $category = $this->findBySlug($slug)->with(['products' => function($query) use ($limit, $offset) {
            $query->orderBy('price');
            $query->limit($limit);
            $query->offset($offset);
        }, 'products.publisher', 'products.authors'])->first();

        DiscountHelper::processProductsPrice($category->products);

        $prices = $this->findBySlug($slug)->with('products')->first();

        $category->min_price = $prices->products->min('price');
        $category->max_price = $prices->products->max('price');

        return $category;

    }

    public function filterProducts($slug, $minPrice, $maxPrice, $author, $publisher, $limit, $offset = 0)
    {
        $category = $this->findBySlug($slug)
                        ->with(['products' => function ($query) use ($minPrice, $maxPrice, $author, $publisher) {
                            $query->where('price', '<=', $maxPrice);
                            $query->where('price', '>=', $minPrice);
                            if ($publisher) {
                                $query->whereIn('publisher_id',  $publisher);
                            }
                            if($author) {
                                $query->whereHas('authors', function ($query) use ($author) {
                                    $query->whereIn('id', $author);
                                });
                            }
                            $query->orderBy('price');
                        }, 'products.publisher', 'products.authors'])
                        ->first();

        $products = $category->products->slice($offset, $limit);
        $products->countAll = $category->products->count();

        DiscountHelper::processProductsPrice($products);

        return $products;

    }

    public function countProducts($slug)
    {
        $products = $this->findBySlug($slug)->withCount('products')->first();

        return $products->products_count;
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug);
    }

    public function all()
    {
        return $this->model->get();
    }

}