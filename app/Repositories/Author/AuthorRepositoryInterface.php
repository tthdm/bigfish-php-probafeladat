<?php
namespace App\Repositories\Author;

use App\Repositories\RepositoryInterface;

interface AuthorRepositoryInterface extends RepositoryInterface
{

    public function countProducts($slug);

    public function paginateProducts($slug, $limit, $offset);

    public function paginateProductsWithPublisher($slug, $limit, $offset);

    public function findBySlug($slug);

    public function allWithProducts();

}