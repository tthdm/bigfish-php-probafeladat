<?php

namespace App\Repositories\Author;

use App\Author;
use App\Helpers\DiscountHelper;

class AuthorRepository implements AuthorRepositoryInterface
{

    /**
     * The model of authors
     * @var Author
     */
    protected $model;

    public function __construct(Author $model)
    {
        $this->model = $model;
    }

    public function countProducts($slug)
    {
        $products = $this->findBySlug($slug)->withCount('products')->first();

        return $products->products_count;
    }

    public function paginateProducts($slug, $limit, $offset = 0)
    {
        $author = $this->findBySlug($slug)->with(['products' => function ($query) use ($limit, $offset) {
            $query->limit($limit);
            $query->offset($offset);
        }]);

        DiscountHelper::processProductsPrice($author->products);

        return $author;
    }

    public function paginateProductsWithPublisher($slug, $limit, $offset = 0)
    {
        $author = $this->findBySlug($slug)->with(['products' => function ($query) use ($limit, $offset) {
            $query->limit($limit);
            $query->offset($offset);
        }, 'products.publisher'])->first();

        DiscountHelper::processProductsPrice($author->products);

        return $author;
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug);
    }

    public function allWithProducts()
    {
        $authors = $this->model->withCount('products')->get();

        foreach($authors as $author) {
            $author->publishers = $author->products->pluck('publisher_id');
        }

        return $authors;
    }

}