<?php

namespace App\Repositories\Publisher;

use App\Publisher;
use App\Helpers\DiscountHelper;

class PublisherRepository implements PublisherRepositoryInterface
{

    /**
     * The model of publishers
     * @var Publisher
     */
    protected $model;

    public function __construct(Publisher $model)
    {
        $this->model = $model;
    }

    public function countProducts($slug)
    {
        $products = $this->findBySlug($slug)->withCount('products')->first();

        return $products->products_count;
    }

    public function paginateProducts($slug, $limit, $offset = 0)
    {
        $publisher =  $this->findBySlug($slug)->with(['products' => function ($query) use ($limit, $offset) {
            $query->limit($limit);
            $query->offset($offset);
        }])->first();

        DiscountHelper::processProductsPrice($publisher->products);

        return $publisher;
    }

    public function paginateProductsWithAuthors($slug, $limit, $offset = 0)
    {
        $publisher = $this->findBySlug($slug)->with(['products' => function ($query) use ($limit, $offset) {
            $query->limit($limit);
            $query->offset($offset);
        }, 'products.authors'])->first();

        DiscountHelper::processProductsPrice($publisher->products);

        return $publisher;
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug);
    }

    public function allWithProducts()
    {
        $publishers = $this->model->withCount('products')->with('products.authors')->get();

        foreach($publishers as $publisher) {
            $publisher->authors = $publisher->products->pluck('authors')->flatten(1)->pluck('id');
        }

        return $publishers;
    }
}