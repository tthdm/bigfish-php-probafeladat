<?php
namespace App\Repositories\Publisher;

use App\Repositories\RepositoryInterface;

interface PublisherRepositoryInterface extends RepositoryInterface
{

    public function countProducts($slug);

    public function paginateProducts($slug, $limit, $offset);

    public function paginateProductsWithAuthors($slug, $limit, $offset);

    public function findBySlug($slug);

    public function allWithProducts();

}