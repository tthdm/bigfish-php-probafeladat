<?php

namespace App;

use App\Product;
use App\Discount;
use Arr;

class Cart
{

    public static function get()
    {

        $cart = collect(session('cart'));

        $result = self::processCart($cart);

        return $result;

    }

    public static function add($product)
    {
        $currentSession = session('cart');
        $checkProduct = Product::where('slug', $product)->first();
        $error = false;

        if ($checkProduct == null) {
            $error = true;
            $message = trans('messages.errors.cart.add.productNotFound');
        } else if (empty($currentSession)) {
            $cartContent = [
                $product => [
                    'qty' => 1,
                ]
            ];
            $cart = $cartContent;
        }  else {
            if (Arr::has($currentSession, $product)) {
                $qty = $currentSession[$product]['qty'];
                $currentSession[$product]['qty'] = $qty+1;

                $cart = $currentSession;
            } else {
                $currentSession[$product] = [
                    'qty' => 1,
                ];
                $cart = $currentSession;
            }
        }

        if($error) {
            return ['error' => $error, 'message' => $message];
        } else {
            session(['cart' => $cart]);
            return ['error' => $error, 'message' => trans('messages.success.cart.add')];
        }

    }

    public static function update($product, $operation)
    {

        $currentSession = session('cart');
        $checkProduct = Product::where('slug', $product)->first();
        $error = false;

        if (empty($currentSession)) {
            $error = true;
            $message = trans('messages.errors.cart.update.emptyCart');
        } else if ($checkProduct == null) {
            $error = true;
            $message = trans('messages.errors.cart.add.productNotFound');
        } else {
            if (Arr::has($currentSession, $product)) {
                $quantity = $currentSession[$product]['qty'];

                if ($operation == 'remove') {
                    if ($quantity == 1) {
                        $error = true;
                        $message = trans('messages.errors.cart.update.ratherUseDelete');
                    } else {
                        $currentSession[$product]['qty'] = $quantity-1;
                    }
                } else if ($operation == 'add') {
                    $currentSession[$product]['qty'] = $quantity+1;
                } else {
                    $error = true;
                    $message = trans('messages.errors.cart.update.badOperation');
                }
            } else {
                $error = true;
                $message = trans('messages.errors.cart.update.productNotFoundInCart');
            }
        }

        if($error) {
            return ['error' => $error, 'message' => $message];
        } else {
            session(['cart' => $currentSession]);
            return ['error' => $error, 'session' => self::get()];
        }

    }

    public static function remove($product)
    {

        $currentSession = session('cart');
        $checkProduct = Product::where('slug', $product)->first();
        $error = false;

        if ($checkProduct == null) {
            $error = true;
            $message = trans('messages.errors.cart.remove.productNotFound');
        } else if (empty($currentSession)) {
            $error = true;
            $message = trans('messages.errors.cart.remove.emptyCart');

        } else {
            if (Arr::has($currentSession, $product)) {
                unset($currentSession[$product]);
            } else {
                $error = true;
                $message = trans('messages.errors.cart.remove.productNotFoundInCart');
            }
        }

        if($error) {
            return ['error' => $error, 'message' => $message];
        } else {
            session(['cart' => $currentSession]);
            return ['error' => $error, 'session' => self::get()];
        }

    }

    public static function empty() {

        session()->forget('cart');
        $error = false;

        if (!empty(session('cart'))) {
            $error = true;
            $message = trans('messages.errors.cart.empty.couldntEmptyCart');
        }

        if($error) {
            return ['error' => $error, 'message' => $message];
        } else {
            return ['error' => $error];
        }

    }

    private static function calculateCartDiscounts($products, $cart)
    {
        foreach ($products as $product) {
            if ($product->discount()->type == "fix") {
                $product->new_price = $product->price - $product->discount()->value;
            } else if($product->discount()->type == "percent") {
                $product->new_price = $product->price - $product->price * ($product->discount()->value/100);
            } else {
                $discountId = $product->discount()->id;
                $discountProductsIds = Discount::with('products')
                    ->where('id', $discountId)
                    ->first()->products->pluck('id');

                $filteredProducts = $products->whereIn('id', $discountProductsIds);

                if ($filteredProducts->count() >= 3) {
                    $minPrice = $filteredProducts->min('price');
                    $cheapestProduct = $filteredProducts->where('price', $minPrice)->first();
                    if($product->id == $cheapestProduct->id) {
                        $product->new_price = 0;
                    }
                }
            }
        }

        return $products;

    }

    public static function processCart($cart)
    {
        $products = Product::whereIn('slug', $cart->keys())->with(['publisher', 'authors'])->get();

        $products = self::calculateCartDiscounts($products, $cart);

        foreach ($products as $product) {
            if (isset($product->new_price)) {
                $product->cart_price = self::calculateDiscountedPrice($cart, $product);
            } else {
                $product->cart_price = self::calculatePrice($cart, $product);
            }
        }

        $total = 0;
        $discounted = 0;
        $quantity = 0;

        foreach($products as $product) {
            $total += self::calculatePrice($cart, $product);
            $discounted += self::calculateDiscountedPrice($cart, $product);
            $quantity += $cart[$product->slug]['qty'];
        }

        $result = [
            'products' => $products,
            'cart' => $cart,
            'total' => $total,
            'discounted' => $discounted,
            'quantity' => $quantity,
        ];

        return $result;

    }

    public static function calculatePrice($cart, $product) {
        $price = $cart[$product->slug]['qty']*$product->price;

        return $price;
    }

    public static function calculateDiscountedPrice($cart, $product) {

        if(isset($product->new_price)) {
            if($cart[$product->slug]['qty'] > 1 && $product->new_price == 0) {
                $discountedPrice = ($cart[$product->slug]['qty']-1)*$product->price;
            } else {
                $discountedPrice = $cart[$product->slug]['qty']*$product->new_price;
            }
        } else {
            $discountedPrice = $cart[$product->slug]['qty']*$product->price;
        }

        return $discountedPrice;

    }

}