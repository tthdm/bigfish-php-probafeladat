<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function authors()
    {
        return $this->belongsToMany('App\Author', 'product_author', 'product_id', 'author_id');
    }

    public function publisher()
    {
        return $this->hasOne('App\Publisher', 'id', 'publisher_id');
    }

    public function discounts() {
        return $this->belongsToMany('App\Discount', 'product_discount', 'product_id', 'discount_id');
    }

    public function discount() {
        return $this->discounts()->first();
    }

}
