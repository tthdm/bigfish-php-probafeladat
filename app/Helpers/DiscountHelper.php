<?php

namespace App\Helpers;

class DiscountHelper
{

    public static function calculateDiscountedPrice($product)
    {

        if($product->discount()->type == 'fix') {
            $price = $product->price - $product->discount()->value;
        } else if($product->discount()->type == 'percent'){
            $price = $product->price - $product->price * ($product->discount()->value/100);
        } else {
            $price = $product->price;
        }

        return $price;

    }

    public static function processProductsPrice($products)
    {
        foreach ($products as $product) {
            self::attachDiscountedPrice($product);
        }

        return $products;
    }

    public static function attachDiscountedPrice($product)
    {
        if(!empty($product->discounts)) {
            $product->discount = $product->discount();
            $product->new_price = self::calculateDiscountedPrice($product);
        }

        return $product;
    }

}