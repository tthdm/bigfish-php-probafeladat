<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Product;
use App\Cart;

class CartTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {

        $cart1 = collect([
            'java' => [
                'qty' => 1
            ],
            'c-2008' => [
                'qty' => 1
            ],
            'dreamweaver-cs4' => [
                'qty' => 1
            ],
        ]);
        $cart2 = collect([
            'javascript-kliens-oldalon' => [
                'qty' => 3
            ],
            'algoritmusok' => [
                'qty' => 2
            ],
        ]);
        $cart3 = collect([
            'java' => [
                'qty' => 1
            ],
            'c-2008' => [
                'qty' => 1
            ],
            'dreamweaver-cs4' => [
                'qty' => 1
            ],
            'javascript-kliens-oldalon' => [
                'qty' => 1
            ],
            'algoritmusok' => [
                'qty' => 1
            ],
        ]);

        $result1 = Cart::processCart($cart1)['discounted'];
        $result2 = Cart::processCart($cart2)['discounted'];
        $result3 = Cart::processCart($cart3)['discounted'];

        $cart1Total = 7600;
        $cart2Total = 14220;
        $cart3Total = 13510;

        $this->assertTrue($result1 == $cart1Total);
        $this->assertTrue($result2 == $cart2Total);
        $this->assertTrue($result3 == $cart3Total);
    }
}
