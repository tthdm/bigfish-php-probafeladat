#!/bin/sh

domain=$1;
location=$2;

echo '
server {
    listen 80;

    server_name '"$domain"';

    index  index.php;

    charset utf-8;

    root  '"$location"'/public;

    gzip on;
    gzip_static on;
    gzip_http_version 1.0;
    gzip_disable "MSIE [1-6].";
    gzip_vary on;
    gzip_comp_level 9;
    gzip_proxied any;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript image/svg+xml;

    fastcgi_intercept_errors off;
    fastcgi_buffers 8 16k;
    fastcgi_buffer_size 32k;
    fastcgi_read_timeout 180;

    client_max_body_size 120M;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

	location ~ \.php$ {

        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;

        include        fastcgi_params;
    }

    location ~ /\.ht {

        access_log off;
        log_not_found off;

        deny all;
    }

         location ~* \.ico$ {

        expires 1w;
        access_log off;
    }

    location ~* \.(?:jpg|jpeg|gif|png|ico|gz|svg|svgz|ttf|otf|woff|eot|mp4|ogg|ogv|webm)$ {

        try_files $uri $uri/ /index.php?$query_string;

        access_log off;
        log_not_found off;
    }

    location ~* \.(?:css|js)$ {

        try_files $uri $uri/ /index.php?$query_string;

        access_log off;
        log_not_found off;
    }

    add_header "X-UA-Compatible" "IE=Edge,chrome=1";

}' | sudo tee nginx.conf > /dev/null

echo "Az NGINX konfigurációs fájl sikeresen létrejött 'nginx.conf' néven."
