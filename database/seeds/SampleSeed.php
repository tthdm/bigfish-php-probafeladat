<?php

use Illuminate\Database\Seeder;

use App\Product;
use App\Publisher;
use App\Author;
use App\Discount;
use App\Category;

class SampleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Könyv'
            ],
        ];
        $authors = [
            [
                'name' => 'Janine Warner',
            ],
            [
                'name' => 'Sikos László',
            ],
            [
                'name' => 'Barry Burd',
            ],
            [
                'name' => 'Stephen Randy Davis',
            ],
            [
                'name' => 'Joshua Eichorn',
            ],
            [
                'name' => 'Ivanyos Gábor',
            ],
            [
                'name' => 'Rónyai Lajos',
            ],
            [
                'name' => 'Szabó Réka',
            ],
        ];
        $publishers = [
            [
                'name' => 'PANEM',
            ],
            [
                'name' => 'BBS-INFO',
            ],
            [
                'name' => 'TYPOTEX',
            ],
        ];
        $products = [
            [
                'id' => 1001,
                'name' => 'Dreamweaver CS4',
                'price' => 3900,
                'index_image' => '/img/books/dreamweaver_cs4.jpg',
                'author' => ['Janine Warner'],
                'publisher' => 'PANEM',
                'category' => 'Könyv',
            ],
            [
                'id' => 1002,
                'name' => 'JavaScript kliens oldalon',
                'price' => 2900,
                'index_image' => '/img/books/javascript_kliens_oldalon.jpg',
                'author' => ['Sikos László'],
                'publisher' => 'BBS-INFO',
                'category' => 'Könyv',
            ],
            [
                'id' => 1003,
                'name' => 'Java',
                'price' => 3700,
                'index_image' => '/img/books/java.jpg',
                'author' => ['Barry Burd '],
                'publisher' => 'PANEM',
                'category' => 'Könyv',
            ],
            [
                'id' => 1004,
                'name' => 'C# 2008',
                'price' => 3700,
                'index_image' => '/img/books/csharp_2008.jpg',
                'author' => ['Stephen Randy Davis'],
                'publisher' => 'PANEM',
                'category' => 'Könyv',
            ],
            [
                'id' => 1005,
                'name' => 'Az Ajax alapjai',
                'price' => 4500,
                'index_image' => '/img/books/az_ajax_alapjai.jpg',
                'author' => ['Joshua Eichorn'],
                'publisher' => 'PANEM',
                'category' => 'Könyv',
            ],
            [
                'id' => 1006,
                'name' => 'Algoritmusok',
                'price' => 3900,
                'index_image' => '/img/books/algoritmusok.jpg',
                'author' => [
                    'Ivanyos Gábor',
                    'Rónyai Lajos',
                    'Szabó Réka',
                ],
                'publisher' => 'TYPOTEX',
                'category' => 'Könyv',
            ],
        ];
        $discounts = [
            [
                'id' => 101,
                'name' => '10%-os kedvezmény a termék árából',
                'type' => 'percent',
                'value' => '10',
                'restriction' => 'products',
                'restriction_value' => 1006
            ],
            [
                'id' => 102,
                'name' => '500-os kedvezmény a termék árából',
                'type' => 'fix',
                'value' => '500',
                'restriction' => 'products',
                'restriction_value' => 1002,
            ],
            [
                'id' => 103,
                'name' => '2+1 csomag kedvezmény',
                'type' => 'bundle',
                'value' => null,
                'restriction' => 'publishers',
                'restriction_value' => 'panem',
            ]
        ];

        /*
         * Kategóriák adatbázisba töltése
         */

        foreach ($categories as $category) {
            $checkCategory = Category::where('slug', Str::slug($category['name']))->first();

            if ($checkCategory === null) {
                Category::create([
                    'name' => $category['name'],
                    'slug' => Str::slug($category['name']),
                ]);
            }
        }

        /*
         * Szerzők adatbázisba töltése
         */

        foreach ($authors as $author) {
            $checkAuthor = Author::where('slug', Str::slug($author['name']))->first();

            if ($checkAuthor === null) {
                Author::create([
                    'name' => $author['name'],
                    'slug' => Str::slug($author['name']),
                ]);
            }
        }

        /*
         * Kiadók adatbázisba töltése
         */

        foreach ($publishers as $publisher) {
            $checkPublisher = Publisher::where('slug', Str::slug($publisher['name']))->first();

            if ($checkPublisher === null) {
                Publisher::create([
                    'name' => $publisher['name'],
                    'slug' => Str::slug($publisher['name']),
                ]);
            }
        }

        /*
         * Termékek adatbázisba töltése
         */

        foreach ($products as $product) {
            $checkProduct = Product::where('slug', Str::slug($product['name']))
                                        ->first();

            $publisher = Publisher::where('slug', Str::slug($product['publisher']))
                                        ->first();

            if ($checkProduct === null) {
                Product::create([
                    'id' => $product['id'],
                    'name' => $product['name'],
                    'slug' => Str::slug($product['name']),
                    'price' => $product['price'],
                    'index_image' => $product['index_image'],
                    'publisher_id' => $publisher->id,
                ]);

            }

            foreach ($product['author'] as $author) {
                $author = Author::where('slug', Str::slug($author))
                                        ->first();

                $checkAuthorPivot = DB::table('product_author')
                                ->where('author_id', $author->id)
                                ->where('product_id', $product['id'])
                                ->first();

                if ($checkAuthorPivot === null) {
                    DB::table('product_author')->insert([
                        'author_id' => $author->id,
                        'product_id' => $product['id']
                    ]);
                }
            }

            $category = Category::where('slug', Str::slug($product['category']))
                                    ->first();

            $checkCategoryPivot = DB::table('product_category')
                                ->where('category_id', $category->id)
                                ->where('product_id', $product['id'])
                                ->first();

            if ($checkCategoryPivot === null) {
                DB::table('product_category')->insert([
                    'category_id' => $category->id,
                    'product_id' => $product['id']
                ]);
            }

        }

        /*
         * Kedvezmények adatbázisba töltése
         */
        foreach ($discounts as $discount) {
            $checkDiscount = Discount::where('id', $discount['id'])
                                        ->first();

            if($checkDiscount === null) {

                Discount::create([
                    'id' => $discount['id'],
                    'name' => $discount['name'],
                    'type' => $discount['type'],
                    'value' => $discount['value'],
                ]);

                if ($discount['restriction'] == 'products') {
                    $checkDiscountPivot = DB::table('product_discount')
                        ->where('discount_id', $discount['id'])
                        ->where('product_id', $discount['restriction_value'])
                        ->first();

                    if ($checkDiscountPivot === null) {
                        DB::table('product_discount')->insert([
                            'discount_id' => $discount['id'],
                            'product_id' => $discount['restriction_value']
                        ]);
                    }
                } else {
                    $products = Publisher::with(['products'])
                        ->where('slug', $discount['restriction_value'])
                        ->first()->products;

                    foreach ($products as $product) {

                        $checkDiscountPivot = DB::table('product_discount')
                            ->where('discount_id', $discount['id'])
                            ->where('product_id', $product->id)
                            ->first();

                        if ($checkDiscountPivot === null) {
                            DB::table('product_discount')->insert([
                                'discount_id' => $discount['id'],
                                'product_id' => $product->id,
                            ]);
                        }

                    }

                }
            }
        }

    }
}
